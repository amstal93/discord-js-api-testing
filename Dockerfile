FROM node:latest

RUN apt-get update \
    && apt-get install -y --no-install-recommends ffmpeg libopus0 \
    && apt-get autoremove -y \
    && mkdir /app

COPY app/package.json /app

RUN npm install discord.js node-opus

COPY app/app.js /app/
COPY app/resources/ /app/resources

WORKDIR /app

CMD node app.js
